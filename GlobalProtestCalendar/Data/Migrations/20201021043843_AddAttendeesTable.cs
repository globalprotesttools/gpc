﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GlobalProtestCalendar.Data.Migrations
{
    public partial class AddAttendeesTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProtestAttendee",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProtestId = table.Column<Guid>(nullable: false),
                    User = table.Column<string>(maxLength: 64, nullable: true),
                    IsAttending = table.Column<bool>(nullable: false),
                    KnownAs = table.Column<string>(maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProtestAttendee", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProtestAttendee_Protests_ProtestId",
                        column: x => x.ProtestId,
                        principalTable: "Protests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProtestAttendee_ProtestId",
                table: "ProtestAttendee",
                column: "ProtestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProtestAttendee");
        }
    }
}
