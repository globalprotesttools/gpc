﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GlobalProtestCalendar.Data.Migrations
{
    public partial class AddComments : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProtestAttendee_Protests_ProtestId",
                table: "ProtestAttendee");

            migrationBuilder.DropForeignKey(
                name: "FK_ProtestComment_ProtestAttendee_ProtestAttendeeId",
                table: "ProtestComment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProtestComment",
                table: "ProtestComment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProtestAttendee",
                table: "ProtestAttendee");

            migrationBuilder.RenameTable(
                name: "ProtestComment",
                newName: "ProtestComments");

            migrationBuilder.RenameTable(
                name: "ProtestAttendee",
                newName: "ProtestAttendees");

            migrationBuilder.RenameIndex(
                name: "IX_ProtestComment_ProtestAttendeeId",
                table: "ProtestComments",
                newName: "IX_ProtestComments_ProtestAttendeeId");

            migrationBuilder.RenameIndex(
                name: "IX_ProtestAttendee_ProtestId",
                table: "ProtestAttendees",
                newName: "IX_ProtestAttendees_ProtestId");

            migrationBuilder.AddColumn<int>(
                name: "Likes",
                table: "ProtestComments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<Guid>(
                name: "ProtestId",
                table: "ProtestComments",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProtestComments",
                table: "ProtestComments",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProtestAttendees",
                table: "ProtestAttendees",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_ProtestComments_ProtestId",
                table: "ProtestComments",
                column: "ProtestId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProtestAttendees_Protests_ProtestId",
                table: "ProtestAttendees",
                column: "ProtestId",
                principalTable: "Protests",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ProtestComments_ProtestAttendees_ProtestAttendeeId",
                table: "ProtestComments",
                column: "ProtestAttendeeId",
                principalTable: "ProtestAttendees",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ProtestComments_Protests_ProtestId",
                table: "ProtestComments",
                column: "ProtestId",
                principalTable: "Protests",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProtestAttendees_Protests_ProtestId",
                table: "ProtestAttendees");

            migrationBuilder.DropForeignKey(
                name: "FK_ProtestComments_ProtestAttendees_ProtestAttendeeId",
                table: "ProtestComments");

            migrationBuilder.DropForeignKey(
                name: "FK_ProtestComments_Protests_ProtestId",
                table: "ProtestComments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProtestComments",
                table: "ProtestComments");

            migrationBuilder.DropIndex(
                name: "IX_ProtestComments_ProtestId",
                table: "ProtestComments");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProtestAttendees",
                table: "ProtestAttendees");

            migrationBuilder.DropColumn(
                name: "Likes",
                table: "ProtestComments");

            migrationBuilder.DropColumn(
                name: "ProtestId",
                table: "ProtestComments");

            migrationBuilder.RenameTable(
                name: "ProtestComments",
                newName: "ProtestComment");

            migrationBuilder.RenameTable(
                name: "ProtestAttendees",
                newName: "ProtestAttendee");

            migrationBuilder.RenameIndex(
                name: "IX_ProtestComments_ProtestAttendeeId",
                table: "ProtestComment",
                newName: "IX_ProtestComment_ProtestAttendeeId");

            migrationBuilder.RenameIndex(
                name: "IX_ProtestAttendees_ProtestId",
                table: "ProtestAttendee",
                newName: "IX_ProtestAttendee_ProtestId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProtestComment",
                table: "ProtestComment",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProtestAttendee",
                table: "ProtestAttendee",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ProtestAttendee_Protests_ProtestId",
                table: "ProtestAttendee",
                column: "ProtestId",
                principalTable: "Protests",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ProtestComment_ProtestAttendee_ProtestAttendeeId",
                table: "ProtestComment",
                column: "ProtestAttendeeId",
                principalTable: "ProtestAttendee",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
