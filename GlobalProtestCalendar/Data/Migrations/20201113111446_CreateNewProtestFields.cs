﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GlobalProtestCalendar.Data.Migrations
{
    public partial class CreateNewProtestFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Approved",
                table: "Protests",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Protests",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Approved",
                table: "Protests");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Protests");
        }
    }
}
