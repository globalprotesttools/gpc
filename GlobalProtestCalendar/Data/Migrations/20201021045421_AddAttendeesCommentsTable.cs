﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GlobalProtestCalendar.Data.Migrations
{
    public partial class AddAttendeesCommentsTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProtestComment",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ProtestAttendeeId = table.Column<Guid>(nullable: false),
                    Message = table.Column<string>(maxLength: 8096, nullable: true),
                    Updated = table.Column<DateTime>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProtestComment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProtestComment_ProtestAttendee_ProtestAttendeeId",
                        column: x => x.ProtestAttendeeId,
                        principalTable: "ProtestAttendee",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProtestComment_ProtestAttendeeId",
                table: "ProtestComment",
                column: "ProtestAttendeeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProtestComment");
        }
    }
}
