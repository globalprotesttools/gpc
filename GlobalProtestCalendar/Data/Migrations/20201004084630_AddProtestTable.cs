﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GlobalProtestCalendar.Data.Migrations
{
    public partial class AddProtestTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Protests",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    User = table.Column<string>(maxLength: 64, nullable: true),
                    ProtestName = table.Column<string>(maxLength: 64, nullable: false),
                    ProtestDescription = table.Column<string>(maxLength: 16384, nullable: false),
                    Country = table.Column<string>(maxLength: 8, nullable: false),
                    Location = table.Column<string>(maxLength: 64, nullable: false),
                    LocationDetails = table.Column<string>(maxLength: 256, nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    Duration = table.Column<string>(maxLength: 128, nullable: false),
                    ExpectedOutcome = table.Column<string>(maxLength: 128, nullable: false),
                    Motivation = table.Column<int>(nullable: false),
                    CoverImagePath = table.Column<string>(maxLength: 512, nullable: true),
                    CoverImageContentType = table.Column<string>(maxLength: 32, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Protests", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Protests");
        }
    }
}
