using System;
using System.ComponentModel.DataAnnotations;
using GlobalProtestCalendar.Business;
using Microsoft.AspNetCore.Identity;

namespace GlobalProtestCalendar.Data
{
    public class AppUser : IdentityUser
    {
        [Required]
        [MaxLength(Constants.MaxDisplayNameLength)]
        public string DisplayName { get; set; }
        
        public DateTime Created { get; set; }

        public AccountType AccountType { get; set; }
        
        [Required]
        [StringLength(1024)]
        public string Pu { get; set; }
        
        [Required]
        [StringLength(2048)]
        public string Pr { get; set; }
    }
}