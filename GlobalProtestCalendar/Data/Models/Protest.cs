using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using GlobalProtestCalendar.Business;
using GlobalProtestCalendar.Business.Services;
using GlobalProtestCalendar.Models.Protest;
using Microsoft.AspNetCore.Http;

namespace GlobalProtestCalendar.Data.Models
{
    public class Protest
    {
        [Key] public Guid Id { get; set; } = Guid.NewGuid();
        
        [MaxLength(Constants.MaxUserIdHashLength)]
        public string User { get; set; }
        
        [Required]
        [MaxLength(Constants.MaxProtestNameLength)]
        public string ProtestName { get; set; }

        [Required]
        [MaxLength(Constants.MaxProtestDescriptionLength)]
        public string ProtestDescription { get; set; }

        [Required]
        [MaxLength(Constants.MaxCountryNameLength)]
        public string Country { get; set; }
        
        [Required]
        [MaxLength(Constants.MaxLocationNameLength)]
        public string Location { get; set; }

        [Required]
        [MaxLength(Constants.MaxLocationDetailsLength)]
        public string LocationDetails { get; set; }

        public DateTime StartDate { get; set; }

        [Required]
        [MaxLength(Constants.MaxDurationNameLength)]
        public string Duration { get; set; }

        [Required]
        [MaxLength(Constants.MaxExpectedOutcomeLength)]
        public string ExpectedOutcome { get; set; }

        [Required]
        public Motivation Motivation { get; set; }

        [MaxLength(512)]
        public string CoverImagePath { get; set; }
        
        [MaxLength(32)]
        public string CoverImageContentType { get; set; }

        public ICollection<ProtestAttendee> Attendees { get; set; } = new List<ProtestAttendee>();

        public DateTime CreateDate { get; set; }

        public bool Approved { get; set; }
    }
}