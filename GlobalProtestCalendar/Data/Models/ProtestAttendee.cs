using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GlobalProtestCalendar.Business;

namespace GlobalProtestCalendar.Data.Models
{
    public class ProtestComment
    {
        [Key] public Guid Id { get; set; }

        public Guid ProtestAttendeeId { get; set; }

        public ProtestAttendee ProtestAttendee { get; set; }

        public Guid ProtestId { get; set; }

        public Protest Protest { get; set; }
        
        public int Likes { get; set; }
        
        [MaxLength(8096)]
        public string Message { get; set; }

        
        public DateTime Updated { get; set; }
        
        public DateTime Created { get; set; }
    }
    
    public class ProtestAttendee
    {
        [Key] public Guid Id { get; set; }

        public Guid ProtestId { get; set; }

        public Protest Protest { get; set; }
        
        [MaxLength(Constants.MaxUserIdHashLength)]
        public string User { get; set; }

        public bool IsAttending { get; set; }

        [MaxLength(Constants.MaxDisplayNameLength)]
        public string KnownAs { get; set; }

        public ICollection<ProtestComment> ProtestComments { get; set; }
    }
}