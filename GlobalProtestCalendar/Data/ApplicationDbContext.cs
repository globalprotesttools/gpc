﻿using System;
using System.Collections.Generic;
using System.Text;
using GlobalProtestCalendar.Data.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace GlobalProtestCalendar.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        public DbSet<Protest> Protests { get; set; }
        
        public DbSet<ProtestAttendee> ProtestAttendees { get; set; }

        public DbSet<ProtestComment> ProtestComments { get; set; }
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
    }
}
