﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading;
using System.Threading.Tasks;
using GlobalProtestCalendar.Business;
using GlobalProtestCalendar.Business.Services;
using GlobalProtestCalendar.Code;
using GlobalProtestCalendar.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;

namespace GlobalProtestCalendar.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly AsymmetricEncryptor _asymmetricEncryptor;
        private readonly SemetricEncryptor _semetricEncryptor;

        public RegisterModel(
            UserManager<AppUser> userManager,
            SignInManager<AppUser> signInManager,
            ILogger<RegisterModel> logger,
            IEmailSender emailSender,
            AsymmetricEncryptor asymmetricEncryptor,
            SemetricEncryptor semetricEncryptor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _emailSender = emailSender;
            _asymmetricEncryptor = asymmetricEncryptor;
            _semetricEncryptor = semetricEncryptor;
        }

        [BindProperty] public InputModel Input { get; set; } = new InputModel();

        public string ReturnUrl { get; set; }

        public class InputModel
        {
            [Required(ErrorMessage = "DisplayNameRequired")]
            [Display(Name = "DisplayName")]
            public string DisplayName { get; set; }

            [Required(ErrorMessage = "EmailAddressRequired")]
            [EmailAddress]
            [Display(Name = "EmailAddress")]
            public string Email { get; set; }

            [Required(ErrorMessage = "UsernameRequired")]
            [Display(Name = "Username")]
            public string Username { get; set; }

            [Required]
            [StringLength(Constants.MaxPasswordLength, ErrorMessage = "PassphraseNotGoodEnough",
                MinimumLength = Constants.MinPasswordLength)]
            [DataType(DataType.Password)]
            [Display(Name = "Passphrase")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm passphrase")]
            [Compare("Password", ErrorMessage = "PasswordDoesNotMatch")]
            public string ConfirmPassword { get; set; }

            public AccountType AccountType { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            var emailState = ModelState.GetValidationState("Input.Email");
            var usernameState = ModelState.GetValidationState("Input.Username");

            if (Input.AccountType == AccountType.Standard && emailState != ModelValidationState.Valid)
            {
                return Page();
            }

            if (Input.AccountType == AccountType.Anonymous && usernameState != ModelValidationState.Valid)
            {
                return Page();
            }

            var displayNameState = ModelState.GetValidationState("Input.DisplayName");
            var passwordState = ModelState.GetValidationState("Input.Password");
            var confPasswordState = ModelState.GetValidationState("Input.ConfirmPassword");

            if (displayNameState == ModelValidationState.Valid
                && passwordState == ModelValidationState.Valid
                && confPasswordState == ModelValidationState.Valid)
            {
                var identifier = Input.AccountType == AccountType.Standard
                    ? Input.Email
                    : Input.Username;

                // A public key so information can be stored against this user when they aren't online
                var pubPri = _asymmetricEncryptor.GeneratePublicPrivateKeyPairs();
                
                // Encrypt the private key with the users password
                var privateKeyEncrypted = _semetricEncryptor.Encrypt(pubPri.Item2, Input.Password);
                
                var user = new AppUser
                {
                    UserName = identifier, 
                    Email = identifier, 
                    DisplayName = Input.DisplayName,
                    AccountType = Input.AccountType,
                    Pu = pubPri.Item1,
                    Pr = privateKeyEncrypted
                };

                var result = await _userManager.CreateAsync(user, Input.Password);

                if (result.Succeeded)
                {
                    _logger.LogInformation("User created a new account with password.");

                    if (Input.AccountType == AccountType.Standard)
                    {
                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                        var callbackUrl = Url.Page(
                            "/Account/ConfirmEmail",
                            pageHandler: null,
                            values: new {area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl},
                            protocol: Request.Scheme);

                        await _emailSender.SendEmailAsync(Input.Email, "Confirm your email",
                            $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                        if (_userManager.Options.SignIn.RequireConfirmedEmail &&
                            Input.AccountType == AccountType.Standard)
                        {
                            return RedirectToPage("RegisterConfirmation");
                        }
                    }

                    user.EmailConfirmed = true;
                    await _userManager.UpdateAsync(user);
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return LocalRedirect(returnUrl);
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}