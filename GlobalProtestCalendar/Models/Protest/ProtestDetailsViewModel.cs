using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GlobalProtestCalendar.Data.Models;

namespace GlobalProtestCalendar.Models.Protest
{
    public class ProtestDetailsViewModel : NewProtestModel
    {
        public bool IsOwner { get; }

        public bool IsAttending { get; }

        public IList<ProtestComment> Comments { get; }

        public ProtestDetailsViewModel(Data.Models.Protest protest, string link, in bool isAttending, IList<ProtestComment> comments, int confirmedAttendees) : base(protest)
        {
            IsOwner = protest.User == link;
            IsAttending = isAttending;
            Comments = comments;
            ConfirmedAttendees = confirmedAttendees;
        }

        [Display(Name = "ConfirmedAttendees")]
        public int ConfirmedAttendees { get; set; }
    }
}