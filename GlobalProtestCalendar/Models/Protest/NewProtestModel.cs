using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using GlobalProtestCalendar.Business;
using GlobalProtestCalendar.Business.Services;
using Microsoft.AspNetCore.Http;

namespace GlobalProtestCalendar.Models.Protest
{
    public class NewProtestModel : IValidatableObject
    {
        public NewProtestModel()
        {
        }

        public NewProtestModel(Data.Models.Protest protest)
        {
            Id = protest.Id;
            Country = protest.Country;
            Duration = protest.Duration;
            Location = protest.Location;
            ExpectedOutcome = protest.ExpectedOutcome;
            LocationDetails = protest.LocationDetails;
            ProtestDescription = protest.ProtestDescription;
            ProtestName = protest.ProtestName;
            StartDate = protest.StartDate;
            CoverImageUrl = protest.CoverImagePath;

            if (protest.Motivation.HasFlag(Protest.Motivation.Political))
            {
                Motivation = Motivation.Concat(new[] {Protest.Motivation.Political}).ToArray();
            }

            if (protest.Motivation.HasFlag(Protest.Motivation.Corruption))
            {
                Motivation = Motivation.Concat(new[] {Protest.Motivation.Corruption}).ToArray();
            }

            if (protest.Motivation.HasFlag(Protest.Motivation.Economic))
            {
                Motivation = Motivation.Concat(new[] {Protest.Motivation.Economic}).ToArray();
            }
        }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "ProtestName")]
        [StringLength(Constants.MaxProtestNameLength)]
        public string ProtestName { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "ProtestDescription")]
        [StringLength(Constants.MaxProtestDescriptionLength)]
        public string ProtestDescription { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Country")]
        [StringLength(Constants.MaxCountryNameLength)]
        public string Country { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "Location")]
        [StringLength(Constants.MaxLocationNameLength)]
        public string Location { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "LocationDetails")]
        [StringLength(Constants.MaxLocationDetailsLength)]
        public string LocationDetails { get; set; }

        [Display(Name = "StartDate")] public DateTime StartDate { get; set; } = DateTime.UtcNow;

        [Required(ErrorMessage = "Required")]
        [StringLength(Constants.MaxDurationNameLength)]
        public string Duration { get; set; }

        [Required(ErrorMessage = "Required")]
        [Display(Name = "ExpectedOutcome")]
        [StringLength(Constants.MaxExpectedOutcomeLength)]
        public string ExpectedOutcome { get; set; }

        [Display(Name = "Motivation")]
        [Required(ErrorMessage = "NoMotivation")]
        public Motivation[] Motivation { get; set; } = {Protest.Motivation.Political};

        [Display(Name = "CoverImage")] public IFormFile CoverImage { get; set; }

        public string CoverImageUrl { get; set; } = "/img/new-protest.jpeg";

        public Guid Id { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Motivation.Any(m => m == Protest.Motivation.None))
            {
                yield return new ValidationResult("Required", new[] {"Motivation"});
            }

            if (LocalizedCountryList.CountryList.First().Value.All(kvp => kvp.Key != Country))
            {
                yield return new ValidationResult("Required", new[] {nameof(Country)});
            }
        }
    }

    [Flags]
    public enum Motivation
    {
        None = 0,
        Political = 1,
        Economic = 2,
        Corruption = 4
    }
}