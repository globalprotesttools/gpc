using System;

namespace GlobalProtestCalendar.Models.Protest
{
    public class ProtestListItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Duration { get; set; }
        public Motivation Motivation { get; set; }
        public string ExpectedOutcome { get; set; }
        public string StartDate { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
    }
}