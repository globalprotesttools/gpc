using System.Collections.Generic;
using System.Globalization;

namespace GlobalProtestCalendar.Models
{
    public class CultureSwitcherModel
    {
        public CultureInfo CurrentUICulture { get; set; }
        public List<CultureInfo> SupportedCultures { get; set; }
    }
}