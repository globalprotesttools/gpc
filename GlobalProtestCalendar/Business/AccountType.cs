namespace GlobalProtestCalendar.Business
{
    public enum AccountType
    {
        Standard = 0,
        Anonymous = 1
    }
}