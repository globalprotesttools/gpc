using System.Threading.Tasks;
using GlobalProtestCalendar.Code;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace GlobalProtestCalendar.Business.Services
{
    public class EmailSender : IEmailSender
    {
        readonly SmtpConfig _config;

        public EmailSender(IOptions<AppSettings> config, ILogger<EmailSender> logger)
        {
            _config = config.Value.SmtpConfig;
        }

        public async Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            var client = new SendGridClient(_config.ApiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(_config.EmailAddress, _config.Name),
                Subject = subject,
                HtmlContent = htmlMessage
            };
            msg.AddTo(new EmailAddress(email, email));
            
            msg.SetClickTracking(false, false);
            
            await client.SendEmailAsync(msg);
        }
    }
}