using System;
using System.Linq;
using System.Threading.Tasks;
using GlobalProtestCalendar.Data;
using GlobalProtestCalendar.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace GlobalProtestCalendar.Business.Services
{
    public class ProtestAttendanceService
    {
        private readonly ApplicationDbContext _dbContext;

        public ProtestAttendanceService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<bool> ToggleAttendance(Guid protestId, string userHash)
        {
            if (await _dbContext.Protests.AnyAsync(p => p.Id == protestId))
            {
                var user = _dbContext.ProtestAttendees.FirstOrDefault(u => u.ProtestId == protestId && u.User == userHash);

                if (user == null)
                {
                    var a = new ProtestAttendee()
                    {
                        ProtestId = protestId,
                        User = userHash,
                        IsAttending = true
                    };
                    _dbContext.ProtestAttendees.Add(a);
                }
                else
                {
                    user.IsAttending = !user.IsAttending;
                }

                return await _dbContext.SaveChangesAsync() > 0;
            }

            return false;
        }
    }
}