using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Newtonsoft.Json;

namespace GlobalProtestCalendar.Business.Services
{
    public class LocalizedCountryList
    {
        private readonly IWebHostEnvironment _webHost;
        
        public static ConcurrentDictionary<string, IDictionary<string, string>> CountryList = new ConcurrentDictionary<string, IDictionary<string, string>>();

        public LocalizedCountryList(IWebHostEnvironment webHost)
        {
            _webHost = webHost;
        }
        
        public IDictionary<string, string> GetCountryList(RequestCulture requestCulture)
        {
            if (CountryList.TryGetValue(requestCulture.UICulture.Name, out var list))
            {
                return list;
            }

            var langPath = Path.Join(_webHost.ContentRootPath, "lang", requestCulture.UICulture.Name.Replace("-", "_"), "country.json");

            var deserializedList = JsonConvert.DeserializeObject<Dictionary<string, string>>(File.ReadAllText(langPath));
            
            CountryList.TryAdd(requestCulture.UICulture.Name, deserializedList);

            return deserializedList;
        }
    }
}