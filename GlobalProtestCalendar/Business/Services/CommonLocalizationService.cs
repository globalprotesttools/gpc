using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Xml.Linq;
using GlobalProtestCalendar.Resources;
using Microsoft.Extensions.Localization;

namespace GlobalProtestCalendar.Business.Services
{
    public class CommonLocalizationService
    {
        private readonly IStringLocalizer localizer;

        public CommonLocalizationService(IStringLocalizerFactory factory)
        {
            var assemblyName = new AssemblyName(typeof(TextualResources).GetTypeInfo().Assembly.FullName);
            localizer = factory.Create(nameof(TextualResources), assemblyName.Name);
        }

        public string Get(string key, string defaultValue = "", params string[] format)
        {
            var value = localizer[key];

            if (String.IsNullOrEmpty(value))
            {
                value = new LocalizedString(key, defaultValue);
            }
#if DEBUG
            var s = File.ReadAllText(
                "Resources/TextualResources.en.resx");

            var x = XDocument.Parse(s);

            var n = x.Descendants("data").FirstOrDefault(d => d.Attribute("name").Value == key);

            if (n == null)
            {
                if (String.IsNullOrWhiteSpace(defaultValue))
                {
                    defaultValue = key;
                }
                var xElement = new XElement("data", new XAttribute("name", key), new XElement("value", HttpUtility.HtmlEncode(defaultValue)));
                
                x.Descendants().First().Add(xElement);
            }
            else // Remove this else statement once the site is complete. Only really useful during development.
            {
                if (value != HttpUtility.HtmlEncode(defaultValue) && !String.IsNullOrWhiteSpace(defaultValue))
                {
                    n.Value = HttpUtility.HtmlEncode(defaultValue);
                    value = new LocalizedString(key, HttpUtility.HtmlEncode(defaultValue));
                }

                if (String.IsNullOrWhiteSpace(value))
                {
                    if (!String.IsNullOrWhiteSpace(defaultValue))
                    {
                        value = new LocalizedString(key, HttpUtility.HtmlEncode(defaultValue));
                    }
                    else
                    {
                        n.Value = key;
                        value = new LocalizedString(key, key);
                    }
                }
            }

            x.Save(
                "Resources/TextualResources.en.resx");

#endif
            return String.IsNullOrEmpty(value)
                ? key
                : String.Format(value, format);
        }
    }
}