using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using GlobalProtestCalendar.Code;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace GlobalProtestCalendar.Business.Services
{
    public class SemetricEncryptor
    {
        private static readonly Encoding Encoding = Encoding.UTF8;
        private string pepper;

        public SemetricEncryptor(IOptions<AppSettings> settings)
        {
            this.pepper = settings.Value.Pepper;
        }
        
        public string Encrypt(string plainText, string key)
        {
            try
            {
                using (var aes = Aes.Create())
                {
                    aes.KeySize = 256;
                    aes.BlockSize = 128;
                    aes.GenerateIV();
                    
                    // Include a pepper value
                    aes.Key = Encoding.GetBytes(key + pepper).Take(32).ToArray();

                    ICryptoTransform aesEncrypt = aes.CreateEncryptor(aes.Key, aes.IV);
                    byte[] buffer = Encoding.GetBytes(plainText);

                    string encryptedText =
                        Convert.ToBase64String(aesEncrypt.TransformFinalBlock(buffer, 0, buffer.Length));

                    // String mac = "";

                    // mac = BitConverter.ToString(HmacSha256(Convert.ToBase64String(aes.IV) + encryptedText, key))
                    //     .Replace("-", "").ToLower();

                    var keyValues = new Dictionary<string, object>
                    {
                        {"iv", Convert.ToBase64String(aes.IV)},
                        {"value", encryptedText},
                        // {"mac", mac},
                    };

                    return Convert.ToBase64String(Encoding.GetBytes(JsonConvert.SerializeObject(keyValues)));
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error encrypting: " + e.Message);
            }
        }

        public string Decrypt(string plainText, string key)
        {
            try
            {
                using (var aes = Aes.Create())
                {
                    aes.KeySize = 256;
                    aes.BlockSize = 128;
                    aes.Key = Encoding.GetBytes(key + pepper).Take(32).ToArray();

                    // Base 64 decode
                    byte[] base64Decoded = Convert.FromBase64String(plainText);
                    string base64DecodedStr = Encoding.GetString(base64Decoded);

                    // JSON Decode base64Str
                    var payload = JsonConvert.DeserializeObject<Dictionary<string, string>>(base64DecodedStr);

                    aes.IV = Convert.FromBase64String(payload["iv"]);

                    ICryptoTransform aesDecrypt = aes.CreateDecryptor(aes.Key, aes.IV);
                    byte[] buffer = Convert.FromBase64String(payload["value"]);

                    return Encoding.GetString(aesDecrypt.TransformFinalBlock(buffer, 0, buffer.Length));
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error decrypting: " + e.Message);
            }
        }

        byte[] HmacSha256(string data, string key)
        {
            var keyBytes = Encoding.GetBytes(key).Take(64).ToArray();

            using HMACSHA256 hmac = new HMACSHA256(keyBytes);
            
            return hmac.ComputeHash(Encoding.GetBytes(data));
        }

        public string HMacSha(string data, string key)
        {
            var hash = HmacSha256(data, key + pepper);

            return Convert.ToBase64String(hash);
        }
    }
}