using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GlobalProtestCalendar.Data;
using GlobalProtestCalendar.Data.Models;
using GlobalProtestCalendar.Models.Protest;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;

namespace GlobalProtestCalendar.Business.Services
{
    public class ProtestCreationService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private readonly CommonLocalizationService _commonLocalizationService;

        public ProtestCreationService(ApplicationDbContext dbContext, IWebHostEnvironment webHostEnvironment, CommonLocalizationService commonLocalizationService)
        {
            _dbContext = dbContext;
            _webHostEnvironment = webHostEnvironment;
            _commonLocalizationService = commonLocalizationService;
        }

        public async Task<bool> ProtestExists(NewProtestModel model, string secretForeignKey)
        {
            return await _dbContext.Protests.AnyAsync(p => p.Id == model.Id && p.User == secretForeignKey);
        }

        public async Task UpdateProtest(NewProtestModel model, string secretForeignKey)
        {
            var protest = await _dbContext.Protests.FirstAsync(p => p.Id == model.Id);

            protest.Country = model.Country;
            protest.Duration = model.Duration;
            protest.Motivation = model.Motivation.Aggregate((m, m2) => m | m2);
            protest.ExpectedOutcome = model.ExpectedOutcome;
            protest.Location = model.Location;
            protest.LocationDetails = model.LocationDetails;
            protest.ProtestDescription = model.ProtestDescription;
            protest.ProtestName = model.ProtestName;
            protest.StartDate = model.StartDate;

            await SetCoverImage(model, protest);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<int> CreateProtestAsync(NewProtestModel model, string secretForeignKey)
        {
            var newProtest = new Protest()
            {
                User = secretForeignKey,
                Country = model.Country,
                Duration = model.Duration,
                Location = model.Location,
                Motivation = model.Motivation.Aggregate((m, m2) => m | m2),
                ExpectedOutcome = model.ExpectedOutcome,
                LocationDetails = model.LocationDetails,
                ProtestDescription = model.ProtestDescription,
                ProtestName = model.ProtestName,
                StartDate = model.StartDate,
                CoverImagePath = "/img/new-protest.jpeg",
                CoverImageContentType = "image/jpg",
                CreateDate = DateTime.UtcNow,
                Attendees =
                {
                    new ProtestAttendee()
                    {
                        User = secretForeignKey,
                        IsAttending = true,
                        KnownAs = _commonLocalizationService.Get("Organiser")
                    }
                }
            };

            _dbContext.Protests.Add(newProtest);

            var result = await _dbContext.SaveChangesAsync();

            if (result > 0)
            {
                await SetCoverImage(model, newProtest);
            }

            return result;
        }

        private async Task SetCoverImage(NewProtestModel model, Protest newProtest)
        {
            if (model.CoverImage != null)
            {
                newProtest.CoverImageContentType = model.CoverImage.ContentType;

                var covers = Path.Join(_webHostEnvironment.WebRootPath, "img/covers");

                if (!Directory.Exists(covers))
                {
                    Directory.CreateDirectory(covers);
                }

                var imagePath = Path.Join(_webHostEnvironment.WebRootPath, "img/covers", newProtest.Id.ToString() + ".jpg");

                if (File.Exists(imagePath))
                {
                    File.Delete(imagePath);
                }

                await using (var fileStream = new FileStream(imagePath, FileMode.Create))
                {
                    await model.CoverImage.CopyToAsync(fileStream);
                }

                newProtest.CoverImagePath = Path.Join("/", "img/covers", newProtest.Id.ToString() + ".jpg");

                await _dbContext.SaveChangesAsync();
            }
        }
    }
}