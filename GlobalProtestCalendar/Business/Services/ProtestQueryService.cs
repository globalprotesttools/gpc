using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GlobalProtestCalendar.Data;
using GlobalProtestCalendar.Data.Models;
using GlobalProtestCalendar.Models.Protest;
using Microsoft.EntityFrameworkCore;

namespace GlobalProtestCalendar.Business.Services
{
    public class ProtestQueryService
    {
        private readonly ApplicationDbContext _dbContext;

        public ProtestQueryService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IList<ProtestListItem>> GetProtests()
        {
            var start = DateTime.UtcNow.Subtract(TimeSpan.FromDays(60));
            var protests =
                await _dbContext.Protests
                    .Where(p => p.StartDate > start && p.Approved)
                    .OrderByDescending(p => p.StartDate)
                    .Select(p => new ProtestListItem
                    {
                        Id = p.Id,
                        Name = p.ProtestName,
                        Country = p.Country,
                        Description = p.ProtestDescription,
                        StartDate = p.StartDate.ToShortDateString(),
                        ExpectedOutcome = p.ExpectedOutcome,
                        Motivation = p.Motivation,
                        Duration = p.Duration
                    })
                    .ToListAsync();

            return protests;
        }

        public async Task<IList<ProtestListItem>> GetProtests(string hashedUserId)
        {
            var protests =
                await _dbContext.Protests
                    .Where(p => p.User == hashedUserId)
                    .OrderByDescending(p => p.StartDate)
                    .Select(p => new ProtestListItem
                    {
                        Id = p.Id,
                        Name = p.ProtestName,
                        Country = p.Country,
                        Description = p.ProtestDescription,
                        StartDate = p.StartDate.ToShortDateString(),
                        ExpectedOutcome = p.ExpectedOutcome,
                        Motivation = p.Motivation,
                        Duration = p.Duration
                    })
                    .ToListAsync();

            return protests;
        }

        public async Task<Protest> GetProtest(Guid id, string hashedUserId)
        {
            return await _dbContext.Protests
                .FirstOrDefaultAsync(p => p.Id == id && p.User == hashedUserId);
        }

        public async Task<(Protest, bool, IList<ProtestComment>, int)> GetProtestDetails(Guid id, string link)
        {
            var protest = await _dbContext.Protests
                .FirstOrDefaultAsync(p => p.Id == id);

            var confirmedAttendees = await _dbContext.ProtestAttendees.CountAsync(a => a.ProtestId == protest.Id);
            
            var isUserAttending = !String.IsNullOrEmpty(link) && await _dbContext.ProtestAttendees
                .AnyAsync(pa => pa.ProtestId == protest.Id && pa.User == link && pa.IsAttending);

            var comments = await _dbContext.ProtestComments
                .OrderByDescending(c => c.Likes)
                .ThenByDescending(c => c.Created)
                .Where(c => c.ProtestId == protest.Id)
                .Take(Constants.CommentsPageSize)
                .ToListAsync();

            return (protest, isUserAttending, comments, confirmedAttendees);
        }
    }
}