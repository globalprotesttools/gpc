using System.Security.Cryptography;

namespace GlobalProtestCalendar.Code
{
    public class AsymmetricEncryptor
    {
        public (string, string) GeneratePublicPrivateKeyPairs()
        {
            using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
            {
               string publicKey = RSA.ToXmlString(false);
               string privateKey = RSA.ToXmlString(true);

               return (publicKey, privateKey);
            }
        }
    }
}