namespace GlobalProtestCalendar.Business
{
    public static class Constants
    {
        public const int MinPasswordLength = 10;
        public const int MaxPasswordLength = 64;
        public const int MaxDisplayNameLength = 32;
        
        public const int MaxProtestNameLength = 64;
        public const int MaxProtestDescriptionLength = 16384;
        public const int MaxCountryNameLength = 8;
        public const int MaxLocationNameLength = 64;
        public const int MaxLocationDetailsLength = 256;
        public const int MaxDurationNameLength = 128;
        public const int MaxExpectedOutcomeLength = 128;
        public const int SessionTimeoutMinutes = 15;

        
        public const string UserIdHashed = "link";
        public const int MaxUserIdHashLength = 64;
        
        public const int CommentsPageSize = 10;
    }
}