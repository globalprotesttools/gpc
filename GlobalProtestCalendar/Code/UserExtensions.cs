using System;
using System.Linq;
using System.Security.Claims;

namespace GlobalProtestCalendar.Code
{
    public static class UserExtensions
    {
        public static string GetUserId(this ClaimsPrincipal user)
        {
            if (user == null)
            {
                return String.Empty;
            }

            return user.Claims.FirstOrDefault(c => c.Type.Contains("nameidentifier"))?.Value ?? String.Empty;
        }
    }
}