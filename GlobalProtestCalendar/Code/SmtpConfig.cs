namespace GlobalProtestCalendar.Code
{
    public class SmtpConfig
    {
        public string Name { get; set; }
        public string EmailAddress { get; set; }
        public string ApiKey { get; set; }
    }
}