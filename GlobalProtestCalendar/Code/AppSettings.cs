namespace GlobalProtestCalendar.Code
{
    public class AppSettings
    {
        public SmtpConfig SmtpConfig { get; set; }
        
        public string Pepper { get; set; }
    }
}