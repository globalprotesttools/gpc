﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using GlobalProtestCalendar.Business.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GlobalProtestCalendar.Models;

namespace GlobalProtestCalendar.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ProtestQueryService _protestQueryService;

        public HomeController(ILogger<HomeController> logger, ProtestQueryService protestQueryService)
        {
            _logger = logger;
            _protestQueryService = protestQueryService;
        }

        public async Task<IActionResult> Index()
        {
            var protests = await _protestQueryService.GetProtests();
            
            return View(protests);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
