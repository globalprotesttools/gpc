﻿using System.Threading.Tasks;
using GlobalProtestCalendar.Business.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GlobalProtestCalendar.Controllers
{
    public class CommentsController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ProtestCreationService _service;
        private readonly CommonLocalizationService _localizationService;
        private readonly ProtestQueryService _protestQueryService;

        public CommentsController(ILogger<HomeController> logger, ProtestCreationService service, CommonLocalizationService localizationService, ProtestQueryService protestQueryService)
        {
            _logger = logger;
            _service = service;
            _localizationService = localizationService;
            _protestQueryService = protestQueryService;
        }

        [HttpPost]
        public async Task<IActionResult> Index()
        {
            // INCREASE ATTENDING COUNT WHEN USER CLICKS ATTENDING
            return Ok();
        }

    }
}