﻿using System;
using System.Threading.Tasks;
using GlobalProtestCalendar.Business;
using GlobalProtestCalendar.Business.Services;
using GlobalProtestCalendar.Models.Protest;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace GlobalProtestCalendar.Controllers
{
    public class ProtestsController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ProtestCreationService _service;
        private readonly CommonLocalizationService _localizationService;
        private readonly ProtestQueryService _protestQueryService;
        private readonly ProtestAttendanceService _protestAttendanceService;

        public ProtestsController(
            ILogger<HomeController> logger,
            ProtestCreationService service,
            CommonLocalizationService localizationService,
            ProtestQueryService protestQueryService,
            ProtestAttendanceService protestAttendanceService)
        {
            _logger = logger;
            _service = service;
            _localizationService = localizationService;
            _protestQueryService = protestQueryService;
            _protestAttendanceService = protestAttendanceService;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var link = HttpContext.Session.GetString(Constants.UserIdHashed);

            var protests = await _protestQueryService.GetProtests(link);

            return View(protests);
        }

        [HttpGet("/protests/{protestSlugUrl}")]
        public async Task<IActionResult> Index(string protestSlugUrl)
        {
            if (!string.IsNullOrWhiteSpace(protestSlugUrl))
            {
                var i = protestSlugUrl.IndexOf(':');

                if (i > -1 && Guid.TryParse(protestSlugUrl.Substring(i + 1), out var id))
                {
                    var link = HttpContext.Session.GetString(Constants.UserIdHashed);

                    var (protest, isAttending, comments, attendees) = await _protestQueryService.GetProtestDetails(id, link);

                    if (protest != null)
                    {
                        return View("Details", new ProtestDetailsViewModel(protest, link, isAttending, comments, attendees));
                    }
                }
            }

            return NotFound();
        }

        [Authorize]
        [HttpGet]
        public IActionResult New()
        {
            return View(new NewProtestModel());
        }

        [Authorize]
        [HttpGet("/protest/edit/{id}")]
        public async Task<IActionResult> Edit([FromRoute] Guid id)
        {
            var hashedUserId = HttpContext.Session.GetString(Constants.UserIdHashed);

            var protest = await _protestQueryService.GetProtest(id, hashedUserId);

            if (protest == null)
            {
                return NotFound();
            }

            return View("Edit", new NewProtestModel(protest));
        }

        [Authorize]
        [HttpPost("/protest/edit/{id}")]
        public async Task<IActionResult> Edit(NewProtestModel model)
        {
            var hashedUserId = HttpContext.Session.GetString(Constants.UserIdHashed);

            if (await _service.ProtestExists(model, hashedUserId))
            {
                await _service.UpdateProtest(model, hashedUserId);

                return RedirectToAction("Index");
            }

            return NotFound();
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> New(NewProtestModel model)
        {
            if (ModelState.IsValid)
            {
                var secretForeignKey = HttpContext.Session.GetString(Constants.UserIdHashed);

                if (!await _service.ProtestExists(model, secretForeignKey))
                {
                    var result = await _service.CreateProtestAsync(model, secretForeignKey);

                    if (result > 0)
                    {
                        return RedirectToAction("Index");
                    }
                }

                ModelState.AddModelError("Form", _localizationService.Get("CreateProtestError", "Unable to create protest."));
            }

            return View();
        }
        
        [Authorize]
        [HttpPost("/protest/attendance")]
        public async Task<bool> Attendance(Guid protestId)
        {
            var secretForeignKey = HttpContext.Session.GetString(Constants.UserIdHashed);

            return await _protestAttendanceService.ToggleAttendance(protestId, secretForeignKey);
        }
    }
}