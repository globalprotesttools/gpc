﻿$( document ).ajaxStart(function() {
    $('body, .ajax-button').css('cursor', 'progress')
});

$( document ).ajaxStop(function() {
    $('body, .ajax-button').css('cursor', 'default')
});
