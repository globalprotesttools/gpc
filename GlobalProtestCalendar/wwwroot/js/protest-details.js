(function () {

    GPC = window.GPC || {};

    var loggedIn;

    GPC.init = function (isLoggedIn, protestId) {
        loggedIn = isLoggedIn;
        initHandlers(protestId);
    }

    function initHandlers(protestId) {

        $('.attend-buttons').on('click', 'button', function () {

            if (!this.id) return;
            
            var button = $(this);

            if (!loggedIn) {
                button.css('opacity', '.5');
                button.css('cursor', 'progress');
                window.location = '/identity/account/register';
                return;
            }
            var attendeeCount = $('#attendeeCount')[0];
            
            $.post('/protest/attendance', {protestId: protestId}, function (result) {

                if (button[0].id === "attend") {
                    button.hide();
                    $('#attending').show();
                    attendeeCount.innerText = parseInt(attendeeCount.innerText, 10) + 1;
                } else {
                    button.hide();
                    $('#attend').show();
                    attendeeCount.innerText = parseInt(attendeeCount.innerText, 10) - 1;
                }
            });
        });
    }
})();

