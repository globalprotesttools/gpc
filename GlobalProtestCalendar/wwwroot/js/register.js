$('input[name="Input.AccountType"]').on('change', function(e) {
   
   if (e.target.id === 'AccountTypeAnonymous') {
      $('.email > input').prop('disabled', true);
      $('.username > input').prop('disabled', false);
      $('.username').removeClass("hidden");
      $('.email').addClass("hidden");
      $('.email-reset').removeClass('supported').addClass('not-supported');
      $('.comp-anon').removeClass('not-supported').addClass('supported');
   } else {
      $('.email > input').prop('disabled', false);
      $('.username > input').prop('disabled', true);
      $('.username').addClass("hidden");
      $('.email').removeClass("hidden");
      $('.email-reset').addClass('supported').removeClass('not-supported');
      $('.comp-anon').addClass('not-supported').removeClass('supported');
   }
});