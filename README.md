## Global Protest Calendar

The goal of this project is to create a website which allows people around the world to organise protests in a safe and secure way without the fear or being arrested or identified by police or authorities. This is in response to recent protests held in Melbourne (Australia) where police raided the homes of several protest organisers and even some people simply commenting on Facebook they were atteding. We need to give back anonimity to people which Facebook and social media have taken away.


# How it works

The key indreedient to making this site work is how it encrypts linking data, or more specifically, foriegn key relationships. It does this by creating a hash of the users ID using their password as the seed value. This value is then used to create relationships to other data.

# Implemented so fear

* Login/Registration
* Create/Edit protests

# Outstanding tasks

I'm only one person so I really need some help to get this site up. Currently, the major outstanding tasks are:

* Delete protests
* Posting/Managing comments for a protest
* Attendee counter (partially working)
* Posting images from a past or current protest
* Change password functionality to re-encrypt all foriegn key relationships
* Testing site functionality
* Adding translations for Spanish and French languages
* Email notifications

Please message me if you are interested in working on any of these so we can discuss the implemention.

# Tech stack

My development box is a Debian based Unbuntu build, using the following tech stack

* aspnet core / dotnet core
* sqlserver

In order to get a development environment up and running, you'll need two things:

* A working sql server instance, either local or using a free instance in the cloud somewhere
* A SendGrid account for sending email. If you'd like to change the code around so that an SMTP server is not required during development then please send a PR.

And of course, a text editor of your choosing such as VSCode.

# Final notes

Please contact me if you'd like to help out with anything.

# Some screenshots

[Screenshot 1](media/screenshot_1.png) [Screenshot 2](media/screenshot_2.png) [Screenshot 3](media/screenshot_3.png)
